package com.infteh.radioplayer;

import android.content.Context;
import android.media.AudioManager;

public class RadioAudioFocus {
    private static final String LOG_TAG = RadioAudioFocus.class.getSimpleName();

    protected RadioService m_service;
    private AudioManager m_audioManager = null;
    private long m_focusLossTs = 0;

    public enum FocusState {Granted, Ducking, Waiting, Abandoned}
    private FocusState m_state = FocusState.Abandoned;

    private final AudioManager.OnAudioFocusChangeListener m_audioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                    try {
                        switch (focusChange) {
                            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                                if (m_state == FocusState.Granted) {
                                    m_state = FocusState.Ducking;
                                    m_service.m_player.setDuckVolume(true);
                                }
                                break;
                            case AudioManager.AUDIOFOCUS_GAIN:
                                if (m_state == FocusState.Waiting) {
                                    m_state = FocusState.Granted;
                                    if (m_focusLossTs != 0) {
                                        long milliseconds = System.currentTimeMillis() - m_focusLossTs;
                                        if (milliseconds < 1000*60*2) { // 2 minutes waiting
                                            m_service.startForegroundPlaying();
                                        }
                                        m_focusLossTs = 0;
                                    }
                                } else if (m_state == FocusState.Ducking) {
                                    m_state = FocusState.Granted;
                                    m_service.m_player.setDuckVolume(false);
                                }
                                break;
                            default:
                                if (m_state == FocusState.Granted) {
                                    m_state = FocusState.Waiting;
                                    m_service.m_player.pause();
                                    m_focusLossTs = System.currentTimeMillis();
                                } else if (m_state == FocusState.Ducking) {
                                    m_state = FocusState.Waiting;
                                    m_service.m_player.pause();
                                    m_service.m_player.setDuckVolume(false);
                                    m_focusLossTs = System.currentTimeMillis();
                                }
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

    RadioAudioFocus(RadioService service) {
        m_service = service;

        // Get audio manager
        m_audioManager = (AudioManager) m_service.getSystemService(Context.AUDIO_SERVICE);
    }

    public boolean requestFocus() {
        try {
            if (m_state == FocusState.Ducking) {
                m_state = FocusState.Granted;
                m_service.m_player.setDuckVolume(false);
            }
            if (m_state != FocusState.Granted) {
                int result = m_audioManager.requestAudioFocus(m_audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    m_state = FocusState.Granted;
                    m_focusLossTs = 0;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return m_state == FocusState.Granted;
    }

    public boolean abandonFocus() {
        try {
            if (m_state == FocusState.Ducking) {
                m_state = FocusState.Granted;
                m_service.m_player.setDuckVolume(false);
            }
            if (m_state == FocusState.Granted) {
                int result = m_audioManager.abandonAudioFocus(m_audioFocusChangeListener);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    m_state = FocusState.Abandoned;
                    m_focusLossTs = 0;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return m_state == FocusState.Abandoned;
    }
}
