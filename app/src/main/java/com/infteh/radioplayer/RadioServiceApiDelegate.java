package com.infteh.radioplayer;

import android.support.v4.media.MediaMetadataCompat;
import android.util.Log;

import java.util.ArrayList;

class RadioServiceApiDelegate extends RadioPlayerApiDelegate {
    private static final String LOG_TAG = RadioServiceApiDelegate.class.getSimpleName();

    private final RadioService m_radioService;

    public RadioServiceApiDelegate(RadioService m_radioService) {
        this.m_radioService = m_radioService;
    }

    @Override
    public void regionResult(RadioPlayerRegion radioPlayerRegion, String s) {}
    @Override
    public void regionsResult(ArrayList<RadioPlayerRegion> arrayList, String s) {}
    @Override
    public void stationsResult(ArrayList<RadioPlayerStation> arrayList, String s) {
        if (!s.isEmpty()) {
            Log.e(LOG_TAG,"Stations update error " + s);
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            m_radioService.m_api.getStations(RadioService.CURRENT_REGION_ID);
                        }
                    },
                    5000
            );
        } else {
            m_radioService.m_stations = arrayList;

            m_radioService.m_metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS,
                    m_radioService.m_stations.size());
            m_radioService.m_mediaSession.setMetadata(m_radioService.m_metadataBuilder.build());

            m_radioService.notifyChildrenChanged(RadioService.RADIO_MEDIA_ROOT_ID);
            if (!m_radioService.m_stations.isEmpty()) {
                m_radioService.updateCurrentStation(m_radioService.m_stations.get(0));
            }
        }
    }
    @Override
    public void optionsResult(String s, String s1, long l, String s2) {}
    @Override
    public void messageResult(String s) {}
    @Override
    public void favoriteResult(String s) {}
}
