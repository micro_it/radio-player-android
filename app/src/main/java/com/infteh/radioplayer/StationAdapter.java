package com.infteh.radioplayer;

import android.content.Context;
import android.support.v4.media.MediaBrowserCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class StationAdapter extends RecyclerView.Adapter<StationAdapter.ViewHolder> {
    private final LayoutInflater m_inflater;
    private final List<MediaBrowserCompat.MediaItem> m_stations;

    StationAdapter(Context context, List<MediaBrowserCompat.MediaItem> stations) {
        m_stations = stations;
        m_inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public StationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = m_inflater.inflate(R.layout.station_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StationAdapter.ViewHolder holder, int position) {
        MediaBrowserCompat.MediaItem station = m_stations.get(position);
        holder.logoView.getBackground().setAlpha(7);
        holder.titleView.setText(station.getDescription().getTitle());
        holder.subtitleView.setText(station.getDescription().getSubtitle());
        holder.rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStationClicked(station);
            }
        });
    }

    @Override
    public int getItemCount() {
        return m_stations.size();
    }

    public abstract void onStationClicked(MediaBrowserCompat.MediaItem station);

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final View rowView;
        final ImageView logoView;
        final TextView titleView, subtitleView;
        ViewHolder(View view){
            super(view);
            rowView = view;
            logoView = (ImageView)view.findViewById(R.id.station_logo);
            titleView = (TextView) view.findViewById(R.id.station_title);
            subtitleView = (TextView) view.findViewById(R.id.station_subtitle);
        }
    }
}
