package com.infteh.radioplayer;

import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;

class RadioServiceSessionCallback extends MediaSessionCompat.Callback {
    private final RadioService m_radioService;

    public RadioServiceSessionCallback(RadioService radioService) {
        this.m_radioService = radioService;
    }

    @Override
    public void onPlay() {
        super.onPlay();

        if (m_radioService.m_currentStation != null
                && m_radioService.m_currentStation.getId() <= 0
                && !m_radioService.m_stations.isEmpty()) {
            m_radioService.updateCurrentStation(m_radioService.m_stations.get(0));
        }

        m_radioService.startForegroundPlaying();
    }

    @Override
    public void onPlayFromMediaId(String mediaId, Bundle extras) {
        super.onPlayFromMediaId(mediaId, extras);

        for (RadioPlayerStation station : m_radioService.m_stations) {
            if (String.valueOf(station.getId()).equals(mediaId)) {
                m_radioService.updateCurrentStation(station);
                break;
            }
        }

        m_radioService.startForegroundPlaying();
    }

    @Override
    public void onPlayFromSearch(String query, Bundle extras) {
        super.onPlayFromSearch(query, extras);
        // https://developer.android.com/guide/topics/media-apps/interacting-with-assistant#parse_voice

        for (RadioPlayerStation station : m_radioService.m_stations) {
            if (station.getName().contains(query)) {
                m_radioService.updateCurrentStation(station);
                break;
            }
        }

        m_radioService.startForegroundPlaying();
    }

    @Override
    public void onPause() {
        super.onPause();
        m_radioService.m_player.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        m_radioService.m_player.stop();
    }

    @Override
    public void onSeekTo(long pos) {
        super.onSeekTo(pos);
        if (m_radioService.m_player.isSeekable()) {
            m_radioService.m_player.setPosition(pos/1000.0);
        }
    }
}
