package com.infteh.radioplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ComponentName;
import android.media.AudioManager;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static android.media.session.PlaybackState.STATE_PLAYING;
import static android.support.v4.media.MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private MediaBrowserCompat m_mediaBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.stations_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Create MediaBrowserServiceCompat
        m_mediaBrowser = new MediaBrowserCompat(this,
                new ComponentName(this, RadioService.class),
                m_connectionCallbacks,
                null); // optional Bundle

        m_mediaBrowser.subscribe(RadioService.RADIO_MEDIA_ROOT_ID,
                new MediaBrowserCompat.SubscriptionCallback() {
                    @Override
                    public void onChildrenLoaded(@NonNull String parentId, @NonNull List<MediaBrowserCompat.MediaItem> stations) {
                        updateStationsView(stations);
                    }

                    @Override
                    public void onError(@NonNull String parentId) {
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        m_mediaBrowser.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    public void onStop() {
        super.onStop();
        // (see "stay in sync with the MediaSession")
        if (MediaControllerCompat.getMediaController(MainActivity.this) != null) {
            MediaControllerCompat.getMediaController(MainActivity.this).unregisterCallback(m_controllerCallback);
        }
        m_mediaBrowser.disconnect();
    }

    protected void buildTransportControls() {
        // Grab the view for the play/pause button
        View playPauseView = (View) findViewById(R.id.panel_play_pause_view);

        // Attach a listener to the button
        playPauseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Since this is a play/pause button, you'll need to test the current state
                // and choose the action accordingly
                MediaControllerCompat mediaController = MediaControllerCompat.getMediaController(MainActivity.this);
                int pbState = mediaController.getPlaybackState().getState();
                if (pbState == PlaybackStateCompat.STATE_PLAYING) {
                    mediaController.getTransportControls().pause();
                } else {
                    mediaController.getTransportControls().play();
                }
            }
        });

        MediaControllerCompat mediaController = MediaControllerCompat.getMediaController(MainActivity.this);

        // Display the initial state
        MediaMetadataCompat metadata = mediaController.getMetadata();
        updatePanelMeta(metadata);

        PlaybackStateCompat pbState = mediaController.getPlaybackState();
        updatePanelPlayButton(pbState);

        // Register a Callback to stay in sync
        mediaController.registerCallback(m_controllerCallback);
    }

    protected void updatePanelMeta(MediaMetadataCompat metadata) {
        TextView panelTitleView = (TextView)findViewById(R.id.panel_title_view);
        panelTitleView.setText(metadata.getText(METADATA_KEY_DISPLAY_TITLE));
        TextView panelSubtitleView = (TextView)findViewById(R.id.panel_subtitle_view);
        panelSubtitleView.setText(metadata.getText(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE));
    }

    protected void updatePanelPlayButton(PlaybackStateCompat pbState) {
        ImageView playPauseImage = (ImageView) findViewById(R.id.panel_play_pause_image);
        if (pbState.getState() == PlaybackStateCompat.STATE_PLAYING || pbState.getState() == PlaybackStateCompat.STATE_BUFFERING) {
            playPauseImage.setImageResource(R.drawable.ic_stop);
        } else {
            playPauseImage.setImageResource(R.drawable.ic_play);
        }
    }

    protected void updateStationsView(@NonNull List<MediaBrowserCompat.MediaItem> stations) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.stations_view);
        StationAdapter adapter = new StationAdapter(this, stations) {
            @Override
            public void onStationClicked(MediaBrowserCompat.MediaItem station) {
                MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls().playFromMediaId(station.getMediaId(), null);
            }
        };
        recyclerView.setAdapter(adapter);
    }

    MediaControllerCompat.Callback m_controllerCallback =
            new MediaControllerCompat.Callback() {
                @Override
                public void onMetadataChanged(MediaMetadataCompat metadata) {
                    updatePanelMeta(metadata);
                }

                @Override
                public void onPlaybackStateChanged(PlaybackStateCompat pbState) {
                    updatePanelPlayButton(pbState);
                }
            };

    private final MediaBrowserCompat.ConnectionCallback m_connectionCallbacks =
            new MediaBrowserCompat.ConnectionCallback() {
                @Override
                public void onConnected() {
                    // Get the token for the MediaSession
                    MediaSessionCompat.Token token = m_mediaBrowser.getSessionToken();

                    // Create a MediaControllerCompat
                    MediaControllerCompat mediaController =
                            new MediaControllerCompat(MainActivity.this, // Context
                                    token);

                    // Save the controller
                    MediaControllerCompat.setMediaController(MainActivity.this, mediaController);

                    // Finish building the UI
                    buildTransportControls();
                }

                @Override
                public void onConnectionSuspended() {
                    // The Service has crashed. Disable transport controls until it automatically reconnects
                }

                @Override
                public void onConnectionFailed() {
                    // The Service has refused our connection
                }
            };
}