package com.infteh.radioplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.media.MediaBrowserServiceCompat;
import androidx.media.session.MediaButtonReceiver;

import com.infteh.startrekplayer.StartrekAndroid;
import com.infteh.startrekplayer.StartrekMetadataUpdater;
import com.infteh.startrekplayer.StartrekMetadataUpdaterDelegate;
import com.infteh.startrekplayer.StartrekNetwork;
import com.infteh.startrekplayer.StartrekPlayer;
import com.infteh.startrekplayer.StartrekPlayerDelegate;
import com.infteh.startrekplayer.StartrekPlayerQuality;
import com.infteh.startrekplayer.StartrekPlayerState;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.support.v4.media.MediaBrowserCompat.MediaItem.FLAG_PLAYABLE;

public class RadioService extends MediaBrowserServiceCompat {
    static {
        System.loadLibrary("StartrekPlayerNative" + StartrekAndroid.PREFERRED_ABI);
        System.loadLibrary("RadioPlayerNative" + StartrekAndroid.PREFERRED_ABI);
    }

    private static final String LOG_TAG = RadioService.class.getSimpleName();

    protected static final String RADIO_MEDIA_ROOT_ID = "radio_root_id";
    protected static final String RADIO_EMPTY_MEDIA_ROOT_ID = "radio_empty_root_id";

    protected static final Integer CURRENT_REGION_ID = 40;

    protected RadioPlayerApi m_api = null;
    protected StartrekPlayer m_player = null;
    protected StartrekMetadataUpdater m_metaUpdater = null;
    protected MediaSessionCompat m_mediaSession = null;
    protected PlaybackStateCompat.Builder m_playbackStateBuilder = new PlaybackStateCompat.Builder();
    protected MediaMetadataCompat.Builder m_metadataBuilder = new MediaMetadataCompat.Builder();
    protected RadioBecomingNoisyReceiver m_noisyReceiver = null;
    protected RadioAudioFocus m_audioFocus = null;
    protected RadioNotificationManager m_notificationManager = null;
    protected boolean m_isForeground = false;

    protected ArrayList<RadioPlayerStation> m_stations;
    RadioPlayerStation m_currentStation = null;

    @Override
    public void onCreate() {
        super.onCreate();

        // Set ca certificates for https connections
        StartrekNetwork.setCaCertificates(StartrekAndroid.getSSLCertificates());

        // Set audio session id
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int sid = audioManager.generateAudioSessionId();
        StartrekAndroid.setAudioSessionId(sid);

        // Create api
        m_api = RadioPlayerApi.create();
        m_api.setDelegate(new RadioServiceApiDelegate(this));
        m_api.getStations(CURRENT_REGION_ID);

        // Create player
        m_player = StartrekPlayer.create();
        m_player.setDelegate(new RadioPlayerDelegate());

        // Create meta updater
        m_metaUpdater = StartrekMetadataUpdater.create();
        m_metaUpdater.setDelegate(new RadioMetadataUpdaterDelegate());

        // Create a MediaSessionCompat
        m_mediaSession = new MediaSessionCompat(this, LOG_TAG);

        // Enable callbacks from MediaButtons and TransportControls
        m_mediaSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player
        m_playbackStateBuilder.setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PLAY_PAUSE);
        m_mediaSession.setPlaybackState(m_playbackStateBuilder.build());

        m_mediaSession.setMetadata(m_metadataBuilder.build());

        // MySessionCallback() has methods that handle callbacks from a media controller
        m_mediaSession.setCallback(new RadioServiceSessionCallback(this));


        // Set activity for media session
        Intent activityIntent = new Intent(this, MainActivity.class);
        m_mediaSession.setSessionActivity(
                PendingIntent.getActivity(this, 0, activityIntent, 0));

        // Response for media buttons
        Intent mediaButtonIntent = new Intent(
                Intent.ACTION_MEDIA_BUTTON, null, this, MediaButtonReceiver.class);
        m_mediaSession.setMediaButtonReceiver(
                PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0));

        // Set the session's token so that client activities can communicate with it.
        setSessionToken(m_mediaSession.getSessionToken());

        // Notification manager
        m_notificationManager = new RadioNotificationManager(this);
        m_notificationManager.register();

        // Noisy receiver
        m_noisyReceiver = new RadioBecomingNoisyReceiver(this);

        // Audio focus
        m_audioFocus = new RadioAudioFocus(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        m_player.stop();
        m_notificationManager.unregister();
        m_noisyReceiver.unregister();
        m_audioFocus.abandonFocus();
        m_mediaSession.release();

        // Delete delegates
        m_api.setDelegate(null);
        m_player.setDelegate(null);
        m_metaUpdater.setDelegate(null);

        // Delete native objects
        m_api = null;
        m_player = null;
        m_metaUpdater = null;
    }

    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid, @Nullable Bundle rootHints) {
        // (Optional) Control the level of access for the specified package name.
        // You'll need to write your own logic to do this.
        if (allowBrowsing(clientPackageName, clientUid)) {
            // Returns a root ID that clients can use with onLoadChildren() to retrieve
            // the content hierarchy.
            return new BrowserRoot(RADIO_MEDIA_ROOT_ID, null);
        } else {
            // Clients can connect, but this BrowserRoot is an empty hierachy
            // so onLoadChildren returns nothing. This disables the ability to browse for content.
            return new BrowserRoot(RADIO_EMPTY_MEDIA_ROOT_ID, null);
        }
    }

    @Override
    public void onLoadChildren(@NonNull String parentId, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
            //  Browsing not allowed
            if (TextUtils.equals(RADIO_EMPTY_MEDIA_ROOT_ID, parentId)) {
                result.sendResult(null);
                return;
            }

            // Check if this is the root menu:
            if (RADIO_MEDIA_ROOT_ID.equals(parentId)) {
                // Build the MediaItem objects for the top level,
                // and put them in the mediaItems list...
                ArrayList<MediaBrowserCompat.MediaItem> data =
                        new ArrayList<>();
                if (m_stations == null || m_stations.isEmpty()) {
                    result.sendResult(data);
                    return;
                }

                for (RadioPlayerStation station : m_stations) {
                    MediaDescriptionCompat.Builder descriptionBuilder =
                            new MediaDescriptionCompat.Builder();
                    descriptionBuilder.setMediaId(String.valueOf(station.getId()));
                    descriptionBuilder.setTitle(station.getName());
                    if (station.getFrequency() > 1) {
                        descriptionBuilder.setSubtitle(String.format(Locale.ENGLISH,"%.2f FM", station.getFrequency()));
                    }
                    descriptionBuilder.setIconUri(Uri.parse(station.getLogoColored()));

                    data.add(new MediaBrowserCompat.MediaItem(descriptionBuilder.build(), FLAG_PLAYABLE));
                }

                result.sendResult(data);
            } else {
                // Examine the passed parentMediaId to see which submenu we're at,
                // and put the children of that menu in the mediaItems list...
                result.sendResult(null);
            }
    }

    @Override
    public int onStartCommand(Intent startIntent, int flags, int startId) {
        int ret = super.onStartCommand(startIntent, flags, startId);
        MediaButtonReceiver.handleIntent(m_mediaSession, startIntent);

        m_notificationManager.updateNotification(true);
        if (m_notificationManager.m_notification != null) {
            startForeground(RadioNotificationManager.NOTIFICATION_ID, m_notificationManager.m_notification);
        }
        m_player.play();

        return ret;
    }

    protected void updateCurrentStation(RadioPlayerStation station) {
        m_currentStation = station;
        m_player.setStreamUrl(m_currentStation.getMobileSourceUrl());
        m_metaUpdater.setMetaUrl(m_currentStation.getMetadataJsonUrl());
        m_metaUpdater.setMetaIdUrl(m_currentStation.getMetadataIdJsonUrl());

        m_metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, m_currentStation.getId());
        m_mediaSession.setMetadata(m_metadataBuilder.build());
    }


    public void startForeground() {
        m_notificationManager.updateNotification(true);
        if (m_notificationManager.m_notification != null && !m_isForeground) {
            startForeground(RadioNotificationManager.NOTIFICATION_ID, m_notificationManager.m_notification);
            m_isForeground = true;
        }
    }

    public void stopForeground() {
        if (m_isForeground) {
            stopSelf();
            stopForeground(false);
            m_isForeground = false;
        }
    }

    protected void startForegroundPlaying() {
        if (!m_audioFocus.requestFocus()) {
            m_player.stop();
            return;
        }
        m_player.play();
        m_mediaSession.setActive(true);
        startForeground();
    }

    private boolean allowBrowsing(String clientPackageName, int clientUid) {
        // NOTE: Android auto should be allowed here
        return clientPackageName.contains("com.infteh");
    }

    protected class RadioPlayerDelegate extends StartrekPlayerDelegate {
        @Override
        public void ended() {}
        @Override
        public void error(String s) {}
        @Override
        public void streamUrlChanged(String s) {}
        @Override
        public void isRestartedChanged(boolean b) {}
        @Override
        public void isHlsChanged(boolean b) {}
        @Override
        public void isSeekableChanged(boolean b) {}
        @Override
        public void stateChanged(StartrekPlayerState startrekPlayerState) {
            int playbackState;
            long actions = PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH | PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
            switch (startrekPlayerState) {
                case PLAYING_STATE:
                    playbackState = PlaybackState.STATE_PLAYING;
                    actions |= PlaybackStateCompat.ACTION_STOP;
                    break;
                case STALLED_STATE:
                    playbackState = PlaybackState.STATE_BUFFERING;
                    actions |= PlaybackStateCompat.ACTION_STOP;
                    break;
                case PAUSED_STATE:
                    playbackState = PlaybackState.STATE_PAUSED;
                    break;
                case STOPPED_STATE:
                    playbackState = PlaybackState.STATE_STOPPED;
                    break;
                default:
                    playbackState = PlaybackState.STATE_ERROR;
                    break;
            }

            m_playbackStateBuilder.setActions(actions);
            m_playbackStateBuilder.setState(playbackState, (long) (m_player.position()*1000), (float) m_player.playbackRate());
            m_mediaSession.setPlaybackState(m_playbackStateBuilder.build());

            m_notificationManager.updateNotification();
        }
        @Override
        public void isPlayingChanged(boolean isPlaying) {
            if (isPlaying) {
                m_noisyReceiver.register();
            } else {
                m_noisyReceiver.unregister();
                m_audioFocus.abandonFocus();
                m_mediaSession.setActive(false);
                stopForeground();
            }
        }
        @Override
        public void isStalledChanged(boolean b) {}
        @Override
        public void isPausedChanged(boolean b) {}
        @Override
        public void isStoppedChanged(boolean b) {}
        @Override
        public void lengthChanged(double v) {}
        @Override
        public void bufferedLengthChanged(double v) {}
        @Override
        public void startPositionChanged(double v) {}
        @Override
        public void positionChanged(double v) {}
        @Override
        public void playbackRateChanged(double v) {}
        @Override
        public void metaChanged(String s) {}
        @Override
        public void volumeChanged(double v) {}
        @Override
        public void duckVolumeChanged(boolean b) {}
        @Override
        public void playingBitratesChanged(ArrayList<Integer> arrayList) {}
        @Override
        public void availableBitratesChanged(ArrayList<Integer> arrayList) {}
        @Override
        public void currentBitrateChanged(int i) {}
        @Override
        public void currentQualityChanged(StartrekPlayerQuality startrekPlayerQuality) {}
        @Override
        public void playingBitrateChanged(int i) {}
        @Override
        public void playingQualityChanged(StartrekPlayerQuality startrekPlayerQuality) {}
        @Override
        public void daastUrlChanged(String s) {}
        @Override
        public void daastStarted(String s, String s1, String s2) {}
        @Override
        public void daastError(String s) {}
        @Override
        public void daastSkipped() {}
        @Override
        public void daastEnded() {}
    }

    protected class RadioMetadataUpdaterDelegate extends StartrekMetadataUpdaterDelegate {
        @Override
        public void metaUrlChanged(String s) {}
        @Override
        public void metaIdUrlChanged(String s) {}
        @Override
        public void titleChanged(String title) {
            m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, title);
            m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, title);
            m_mediaSession.setMetadata(m_metadataBuilder.build());

            m_notificationManager.updateNotification();
        }
        @Override
        public void subtitleChanged(String subtitle) {
            m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, subtitle);
            m_mediaSession.setMetadata(m_metadataBuilder.build());

            m_notificationManager.updateNotification();
        }
        @Override
        public void coverUrlChanged(String coverUrl) {
            m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ART_URI, coverUrl);
            m_mediaSession.setMetadata(m_metadataBuilder.build());

            m_notificationManager.updateNotification();
        }
        @Override
        public void ddbIdChanged(int i) {}
        @Override
        public void typeChanged(int i) {}
    }
}
