package com.infteh.radioplayer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.media.session.MediaButtonReceiver;

import java.util.Arrays;
import java.util.Objects;

public class RadioNotificationManager {
    private static final String LOG_TAG = NotificationManager.class.getSimpleName();

    protected static final int NOTIFICATION_ID = 3567;

    private final RadioService m_service;
    protected NotificationManagerCompat m_notificationManager = null;
    protected Notification m_notification = null;
    protected boolean m_isDisplayed = false;

    protected NotificationCompat.Action m_pauseAction = null;
    protected NotificationCompat.Action m_playAction = null;
    protected NotificationCompat.Action m_stopAction = null;

    protected static final String RADIO_NOTIFICATION_DELETED_ACTION = "RADIO_NOTIFICATION_DELETED";
    protected final BroadcastReceiver m_deletedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (m_isDisplayed) {
                m_isDisplayed = false;
                m_service.stopSelf();
            }
        }
    };

    public RadioNotificationManager(RadioService m_radioService) {
        this.m_service = m_radioService;

        m_pauseAction = new NotificationCompat.Action(
                R.drawable.ic_pause,
                m_service.getString(R.string.label_pause),
                MediaButtonReceiver.buildMediaButtonPendingIntent(m_service, PlaybackStateCompat.ACTION_PAUSE)
        );

        m_playAction = new NotificationCompat.Action(
                R.drawable.ic_play,
                m_service.getString(R.string.label_play),
                MediaButtonReceiver.buildMediaButtonPendingIntent(m_service, PlaybackStateCompat.ACTION_PLAY)
        );

        m_stopAction = new NotificationCompat.Action(
                R.drawable.ic_stop,
                m_service.getString(R.string.label_stop),
                MediaButtonReceiver.buildMediaButtonPendingIntent(m_service, PlaybackStateCompat.ACTION_STOP)
        );

        // Create notification channel
        m_notificationManager = NotificationManagerCompat.from(m_service);
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    m_service.getString(R.string.channel_id),
                    m_service.getString(R.string.channel_name),
                    NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setShowBadge(false);
            NotificationManager manager = (NotificationManager) (m_service.getSystemService(Context.NOTIFICATION_SERVICE));
            if (manager != null) {
                manager.createNotificationChannel(notificationChannel);
            }
        }
    }


    public void register() {
        m_service.registerReceiver(m_deletedReceiver, new IntentFilter(RADIO_NOTIFICATION_DELETED_ACTION));
    }

    public void unregister() {
        m_service.unregisterReceiver(m_deletedReceiver);
    }

    public Notification updateNotification() {
        return updateNotification(false);
    }

    public Notification updateNotification(boolean isForceUpdate) {
        if (isForceUpdate || m_isDisplayed) {
            m_notification = getNotification();
            if (m_notification != null) {
                m_notificationManager.notify(NOTIFICATION_ID, m_notification);
                if (!m_isDisplayed) {
                    m_service.startService(new Intent(m_service.getApplicationContext(), RadioService.class));
                    m_isDisplayed = true;
                }
            }
        }
        return m_notification;
    }

    private Notification getNotification() {
        MediaControllerCompat controller = m_service.m_mediaSession.getController();
        MediaMetadataCompat metadata = controller.getMetadata();
        PlaybackStateCompat playbackState = controller.getPlaybackState();

        if (metadata == null || playbackState == null) {
            return null;
        } else {
            MediaDescriptionCompat description = metadata.getDescription();
            Bitmap art = description.getIconBitmap();
            if (art == null) {
                // use a placeholder art while the remote art is being downloaded.
                art = BitmapFactory.decodeResource(m_service.getResources(),
                        R.drawable.ic_cover);
            }

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(m_service, Objects.requireNonNull(m_service.getString(R.string.channel_id)));

            int[] actionsToShowInCompact = {0};

            if ((playbackState.getActions() & PlaybackStateCompat.ACTION_PAUSE) == PlaybackStateCompat.ACTION_PAUSE) {
                notificationBuilder.addAction(m_pauseAction);
            } else if ((playbackState.getActions() & PlaybackStateCompat.ACTION_STOP) == PlaybackStateCompat.ACTION_STOP) {
                notificationBuilder.addAction(m_stopAction);
            } else {
                notificationBuilder.addAction(m_playAction);
            }

            Intent deletedIntent = new Intent(RADIO_NOTIFICATION_DELETED_ACTION);
            PendingIntent deletedPendintIntent = PendingIntent.getBroadcast(m_service, 0, deletedIntent, 0);

            notificationBuilder.setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                    // show play/pause and stop in compact view.
                    .setShowActionsInCompactView(actionsToShowInCompact)
                    .setMediaSession(m_service.m_mediaSession.getSessionToken())
                    .setShowCancelButton(true)
                    .setCancelButtonIntent(deletedPendintIntent));

            notificationBuilder.setShowWhen(false)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
//                    .setOnlyAlertOnce(true)
                    .setDeleteIntent(deletedPendintIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setShowWhen(false)
                    .setContentIntent(controller.getSessionActivity())
                    .setContentTitle(description.getTitle())
                    .setContentText(description.getSubtitle())
                    .setLargeIcon(art)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

            return notificationBuilder.build();
        }
    }
}
