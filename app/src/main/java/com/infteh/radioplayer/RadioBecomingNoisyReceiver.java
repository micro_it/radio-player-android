package com.infteh.radioplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;

public class RadioBecomingNoisyReceiver {
    private static final String LOG_TAG = RadioBecomingNoisyReceiver.class.getSimpleName();

    protected RadioService m_service;

    private class BecomingNoisyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                m_service.m_player.pause();
            }
        }
    }

    private final IntentFilter m_intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
    private final RadioBecomingNoisyReceiver.BecomingNoisyReceiver m_noisyReceiver = new RadioBecomingNoisyReceiver.BecomingNoisyReceiver();
    private boolean m_isRegistred = false;

    RadioBecomingNoisyReceiver(RadioService service) {
        m_service = service;
    }

    public void register() {
        if (!m_isRegistred) {
            m_service.registerReceiver(m_noisyReceiver, m_intentFilter);
            m_isRegistred = true;
        }
    }

    public void unregister() {
        if (m_isRegistred) {
            m_service.unregisterReceiver(m_noisyReceiver);
            m_isRegistred = false;
        }
    }
}
